# Sicred-api
Este projeto foi criado com o propósito de automatizar os testes de API de uma aplicação de gerenciamento de produtos eletrônicos.

## Escopo do projeto
As seguintes rotas fazem parte escopo do projeto

[GET /test](#https://sicredi-desafio-qe.readme.io/reference/get-test)  
[GET /users](#https://sicredi-desafio-qe.readme.io/reference/get-users)  
[POST /auth/login](https://sicredi-desafio-qe.readme.io/reference/post-auth-login)  
[GET /auth/products](https://sicredi-desafio-qe.readme.io/reference/get-auth-products)  
[POST /products/add](https://sicredi-desafio-qe.readme.io/reference/post-products)  
[GET /products](https://sicredi-desafio-qe.readme.io/reference/get-products)  
[GET /products/{id}](https://sicredi-desafio-qe.readme.io/reference/get-products-id)

## Requerimentos de software
Java SDK 17+  
Maven instalado e configurado no classpath

## Execução dos testes

Você pode abrir cada classe de teste localizada em src\test\java e executa-las, mas recomendo que sejam executados via linha de comando.

Os testes podem ser executados diretamente de sua IDE ou via linha de comando.
Caso deseje executa-los via linha de comando, basta acessar o diretório raiz do projeto e executar os seguintes comandos:

Se deseja rodar todos os cenários execute o comando `mvn test` pois este é o comando comum do maven para rodar todos os testes.

Para rodar por grupos definidos basta informa o paramentro `-Dgroups` e o nome do grupo desejado.
Abaixo segue tabela com a separação dos grupos:  

| Grupo              | command                              |
|--------------------|--------------------------------------|
| Checar Servidor    | `mvn test -Dgroups="checkServidor"`  |
| Autenticação       | `mvn test -Dgroups="autenticacao"`   |
| Produto            | `mvn test -Dgroups="produto"`        |


### Report
Para execução do testes e gerar um relatório padrão do maven basta rodar o seguinte comando:  
`mvn surefire-report:report`


### Plano de testes
Os cenários foram divididos em três grupos.  

- Checar Servidor  
- Autenticação  
- Produto

#### Grupo: Checar Servirdor
Responsável por efetuar uma chamada de teste na rota do servidor

Cenário:  

deveChecarServidor:  
Verifica se a API está funcionando corretamente e se está retornando uma resposta válida.

#### Grupo: Autenticação
Responsável por testes que exigem autenticação para seu correto funcionamento.

Cenários:  

deveVerificarSeExisteNulidadeNoCamposUsernamePassword:  
Verifica se a API está funcionando corretamente e se o campos username e password não são vazios ou nulos.

deveLogarComContaValidaComSucesso:  
Verifica se a API está funcionando corretamente, se está retornando uma resposta válida e se o campo token não é nulo.

naoDeveLogarComUsuarioIncorreto:  
Verifica se a API retorna bad request em casos onde o username possui caracteres especiais.

naoDeveLogarComSenhaIncorreta:  
Verifica se a API retorna status code bad request em casos onde o password está incorreto.

deveAcessarProdutosAposAutenticacaoComSucesso:  
Verifica se a API está funcionando corretamente e se está retornando uma resposta válida.

naoDeveAcessarSemEnviarCabecalhoToken:  
Verifica se a API retorna status code forbidden em casos onde não é enviado token no header da request.

naoDeveAcessarComTokenExpirado:  
Verifica se a API retorna status code unauthorized em casos onde o token enviado está expirado.

naoDeveAcessarComTokenInvalido:  
Verifica se a API retorna status code unauthorized em casos onde o token enviado é inválido.

#### Grupo: Produto
Responsável por testes nas rotas de produto que não exigem autenticação

Cenários:  

deveAdicionarProdutoComSucesso:  
Verifica se a API está funcionando corretamente e se está retornando uma resposta válida.

deveConsultarListaDeProdutosCadastradosComSucesso:  
Verifica se a API está funcionando corretamente e se está retornando uma resposta válida.

deveConsultarProdutoPorIdValido:  
Verifica se a API está funcionando corretamente e se está retornando uma resposta válida.

naoDeveConsultarProdutoPorIdZero:  
Verifica se a API retorna status code not found para casos onde o id é igual a zero.

naoDeveConsultarProdutoPorIdMaisQueLimiteCadastrado:  
Verifica se a API retorna status code not found para casos onde o id é maior que o limite cadastrado no sistema.

### Bug

Identificado que para a API POST products/add nos casos onde não é enviado o payload da request o sistema está gerando um novo ID para um produto. o correto seria aprensentar um erro do tipo bad request.

### Melhorias

Identificado que para a API POST products/add o campo "discountPercentage" não está sendo retornado no response body após inserção com sucesso,
Porém como este comportamento está previsto na documentação estamos cadastrando está melhoria para garantirmos que este campo foi inserido com sucesso.